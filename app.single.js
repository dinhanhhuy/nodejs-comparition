var http = require('http');

let counter = 0;

var server = http.createServer(function(req, res) {
    counter++

    console.log(`receive request    ${counter}`)

    res.writeHead(200);
    res.end('Hello Http');
    
    console.log(`end request        ${counter}`)
});

server.listen(8080);
console.log('start server at 8080')