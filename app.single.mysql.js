const http = require('http');

let connection = null;
const connectSql = () => {
    let mysql      = require('mysql');
    connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : 'root!',
        database : 'nodejs'
    });
    connection.connect();
};

const logger = (...prams) => {
    console.log(...prams);
};

const updateSql1 = () => {
    let requestId = Math.random();
    logger(`start query ${requestId}`);
    connection.query('update Counter set count = count + 1', function (error, results, fields) {

        if (error) throw error;
        logger(`updated ${requestId}`);
      });
};

const updateSql2 = () => {
    let requestId = Math.random();
    connection.query('select * from Counter', function (error, results, fields) {
        let count = results[0].count;
        let update = count + 1;
        logger(`${requestId} - ${count}`);
        
        connection.query(`update Counter set count = ${update}`, function (error, results, fields) {
        if (error) throw error;
        logger(`${requestId} - updated`);
        });
    });
}













connectSql()

var server = http.createServer(function(req, res) {
    // updateSql()
    updateSql1()
    res.writeHead(200);
    res.end('Hello Http');
});

server.listen(8080);
console.log('start server at 8080')






















