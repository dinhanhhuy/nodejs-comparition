var cluster = require('cluster');
var http = require('http');
var numCPUs = require('os').cpus().length;

let counter = 0;

if (cluster.isMaster) {
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
  cluster.on('death', function(worker) {
    console.log('worker ' + worker.pid + ' died');
  });
} else {
  // Worker processes have a http server.
  http.Server(function(req, res) {
    counter++

    console.log(`receive request    ${counter}`)

    res.writeHead(200);
    res.end('Hello Http');
    
    console.log(`end request        ${counter}`)
  }).listen(8080);
}
console.log('start server at 8080')